import static org.junit.Assert.*;

import org.junit.Test;

public class ShoppingCartTest {

	
	// Test Case 1: Creating a new cart has 0 items
	@Test
	public void testCreateNewCart() {
		
		// 1. Create a shopping cart
		ShoppingCart cart = new ShoppingCart();
				
		// 2. Check how many items are in it
		int numItems = cart.getItemCount();
				
		// 3. Compare your actual with expected
		// Using the assert: assetEquals(expectedOutput,actualOutput)
		// The assert statement is the same as saying this:
		//		if (numItems == 0) {
		//			System.out.pirntln("PASS")
		//		}
		//		else {
		//			System.out.pirntln("FAIL")
		//		}
		
		assertEquals(0,numItems);
		

	}
	
	// Test Case 2:  An empty cart has 0 items
	@Test
	public void testEmptyCart01() {
		
		// There are many different ways to test for the empty cart.
		
		// OPTION 1
		// -----------------------------
		// 1. You need a cart
		// 2. Then you need to empty the cart
		// 3. Then you need to check how many items are in the cart 
		// 		Expected result = 0
		// 4. Compare expected to actual
		// 5. If expected == actual ===> PASS otherwise FAIL
		
		// 1. You need a cart
		ShoppingCart cart = new ShoppingCart();
		
		// 2. Then you need to empty the cart
		cart.empty();
		
		// 3. Then you need to check how many items are in the cart 
		// 		Expected result = 0
		int numItems = cart.getItemCount();
		
		// 4. Compare expected to actual
		// 5. If expected == actual ===> PASS otherwise FAIL
		assertEquals(0,numItems);
		
	}
	
	@Test
	public void testEmptyCart02() {
		// OPTION 2
		// -------------------------------
		// 1. Make a cart
		// 2. Add something to the cart
		// 3. Get the number of items in the cart  (initialAmount)
		// 4. Empty the cart
		// 5. Get the number of items in the cart	(currentAmount)
		
		// 1. Make the cart
		ShoppingCart cart = new ShoppingCart();
		
		// add items to the cart
		cart.addItem(new Product("iphone", 1000));
		
		// save the number of items
		int initialAmount = cart.getItemCount();
		
		// now empty the cart
		cart.empty();
		
		//  check how many items are left
		int currentAmount = cart.getItemCount();
		
		
		
		// Tests:
		// 1. The current and old amounts are different
		// 2. The current number of items = 0
		assertNotEquals(initialAmount, currentAmount);
		assertEquals(0, currentAmount);
	}
	
	@Test
	public void testEmptyCart03() {
		// 1. You need a cart
		ShoppingCart cart = new ShoppingCart();
		
		// 2. add items to the cart
		cart.addItem(new Product("iphone", 1000));
		cart.addItem(new Product("samsung", 1000));
		
		assertEquals(2, cart.getItemCount());
		
		// 3. now empty the cart and check how many items are left
		cart.empty();

		// 4. Do your assertion
		assertEquals(0, cart.getItemCount());
		
	}
	
	
	

	

	
	@Test
	public void testAddItem01()  {
		
		// VERSION 1:  Bad example
		// This test case is partially correct.
		// It starts with 0 items and adds an item. 
		// Then it correctly tests that there is 1 item in cart
		// Problem is that it doesn't test the "increment" requirement of the specification.
		// See version 2 & 3 for how to fix this.
		
		
		 
		//1. Create a new shopping cart
		ShoppingCart cart = new ShoppingCart();
		
		//2. Add a new item to cart
		cart.addItem(new Product("samsung folding disaster phone", 2000));
		
		//3. Check how many items are in cart
		int numItems = cart.getItemCount();
		
		//4. ASSERT --> compare actual to equal
		assertEquals(1, numItems);
		
	}
	
	@Test
	public void testAddItem02() {
		// VERSION 2:
		// This test case explicitly tests that adding an item increments teh cart by 1. 
		// We do two tests - 
		// 	1. Test that original cart has 0 items
		//  2. Test that new cart has 1 item
		// This version is good - but probably not maintainable in long run. 
		// See version 3 for best answer!
	
		//1. Create a new shopping cart
		ShoppingCart cart = new ShoppingCart();
		
		//2. OPTIONAL: Make cart is empty
		cart.empty();
		
		//3. Do a test to ensure the cart is empty
		assertEquals(0, cart.getItemCount());
		
		//4. Add a new item to cart
		cart.addItem(new Product("samsung folding disaster phone", 2000));
		
		//5. Check how many items are in cart
		int numItems = cart.getItemCount();
		
		//6. ASSERT --> compare actual to equal
		assertEquals(1, numItems);

		
	}
	@Test 
	public void testAddItem03() {
		// VERSION 3:  Best example
		// Make a cart, and get the initial number of items in cart
		// Add an item
		// Get new number of items
		// Compare the two:  do (current - initial).  The difference = 1
		// (This is how you test the increment)
		// Assert that difference = 1
		

		//1. Create a new shopping cart
		ShoppingCart cart = new ShoppingCart();
		
		// 2. Get starting number of items
		int originalAmount = cart.getItemCount();
		
		// 3. Add an item
		cart.addItem(new Product("samsung folding disaster phone", 2000));
		
		// 4. Get new amount
		int newAmount = cart.getItemCount();
		
		// 5. Calculate the difference between the two amounts
		int difference = newAmount - originalAmount;
		
		// 6. Check if difference = 1
		assertEquals(1, difference);
				
	}
	
}
